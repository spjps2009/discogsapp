package com.discogstags;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.PowerManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerFragment;


import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;


public class ReleaseActivity extends Activity implements
        YouTubePlayer.OnInitializedListener {
    public static final String DEVELOPER_KEY = "AIzaSyAEjrd7mB-W3JIVd1yLJjcT9HNWCspm_6A";
    private static final int RECOVERY_DIALOG_REQUEST = 1;
    public static String currVideo = null;
    //LinearLayout videoBox = null;
    public static YouTubePlayerFragment youTubePlayerFragment = null;
    public static ArrayList<String> videos = null;
    //public static int currIndex = 0;
    public static Context mContext = null;
    private ReleaseFragment releaseFragment;
    protected PowerManager.WakeLock mWakeLock;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ReleaseFragment.nextReleaseId = null;
        ReleaseFragment.RELEASE_ID = getIntent().getExtras().getInt("release_id");
        int playlist_id = getIntent().getExtras().getInt("playlist_id", -1);
        if(playlist_id != -1){
            Uri.Builder uribuilder = new Uri.Builder()
                    .scheme("http")
                    .authority("ec2-52-0-89-113.compute-1.amazonaws.com")
                    .appendPath("home")
                    .appendPath("getreleaseids");
            uribuilder.appendQueryParameter("playlistid", playlist_id+"");
            Uri uri = uribuilder.build();
            String url = uri.toString();
            new RetrievePlaylist().execute(url);
            getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
            getActionBar().hide();
            mContext = ReleaseActivity.this;
            setContentView(R.layout.activity_release);

            //setContentView(R.layout.activity_release);
            return;
            //retrieve
            //set to release_ids
        }
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        getActionBar().hide();
        mContext = this;
        setContentView(R.layout.activity_release);
        if (savedInstanceState == null) {
            releaseFragment = new ReleaseFragment();
            getFragmentManager().beginTransaction()
                    .add(R.id.container, releaseFragment)
                    .commit();
        }

        youTubePlayerFragment =
                (YouTubePlayerFragment) getFragmentManager().findFragmentById(R.id.youtube_fragment);
        if(youTubePlayerFragment == null){
            Log.e("null error", "error");
        }
        if(DEVELOPER_KEY == null){
            Log.e("null error", "error2");
        }
        if(this == null){
            Log.e("error 3", "null");
        }
        final PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        this.mWakeLock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK, "My Tag");
        this.mWakeLock.acquire();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        //new YouTubePageStreamUriGetter().execute("https://www.youtube.com/watch?v=4GuqB1BQVr4");


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.release, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
        Log.e("here2", "");
        if (!b) {
            youTubePlayer.loadVideo("0krTFONiztY");
            youTubePlayer.setPlayerStateChangeListener(new YouTubePlayer.PlayerStateChangeListener() {
                @Override
                public void onLoading() {

                }

                @Override
                public void onLoaded(String s) {

                }

                @Override
                public void onAdStarted() {

                }

                @Override
                public void onVideoStarted() {

                }

                @Override
                public void onVideoEnded() {

                }

                @Override
                public void onError(YouTubePlayer.ErrorReason errorReason) {

                }
            });
        }

        //ft.hide(youTubePlayerFragment);

                //videoBox.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult errorReason) {
        if (errorReason.isUserRecoverableError()) {
            errorReason.getErrorDialog(this, RECOVERY_DIALOG_REQUEST).show();
        } else {
            String errorMessage = String.format(getString(R.string.error_player), errorReason.toString());
            Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        if(releaseFragment == null){
            youTubePlayerFragment =
                    (YouTubePlayerFragment) getFragmentManager().findFragmentById(R.id.youtube_fragment);
        }
        boolean back = releaseFragment.backButtonWasPressed();
        if(!back){
            super.onBackPressed();
        }
    }

    @Override
    public void onDestroy() {
        //this.mWakeLock.release();
        super.onDestroy();
    }

    private class RetrievePlaylist extends AsyncTask<String, Void, String> {

        private Exception exception;

        protected String doInBackground(String... urls) {
            Log.e("url", urls[0]);
            HttpResponse response = getURL(urls[0]);
            Log.e("response", response.getStatusLine().toString());
            try {
                InputStream content = response.getEntity().getContent();
                String jsonString = convertStreamToString(content);
                return jsonString;
            } catch (IOException e) {
                Log.e("error in do in bg", e.getMessage());
            }
            return null;
        }
        protected void onPostExecute(String result) {
            JSONObject json = null;
            try {
                json = new JSONObject(result);
                Log.e("json", json.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JSONArray release_idsJSON = null;
            try {
                release_idsJSON = json.getJSONArray("playlists");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            ArrayList<Integer> releases = new ArrayList<>();
            for(int i = 0; i < release_idsJSON.length(); i++){
                try {
                    releases.add(release_idsJSON.getInt(i));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            ReleaseFragment.RELEASE_ID = releases.remove(0);
            ReleaseFragment.nextReleaseId = releases;
            releaseFragment = new ReleaseFragment();
            getFragmentManager().beginTransaction()
                    .add(R.id.container, releaseFragment)
                    .commit();

            youTubePlayerFragment =
                    (YouTubePlayerFragment) getFragmentManager().findFragmentById(R.id.youtube_fragment);
            if(youTubePlayerFragment == null){
                Log.e("null error", "error");
            }
            if(DEVELOPER_KEY == null){
                Log.e("null error", "error2");
            }
            if(this == null){
                Log.e("error 3", "null");
            }
            final PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);


        }

        private HttpResponse getURL(String url){
            HttpResponse response = null;
            try {
                HttpClient client = new DefaultHttpClient();
                HttpGet request = new HttpGet();
                URI uri = new URI(url);
                request.setURI(uri);
                response = client.execute(request);
            }catch (URISyntaxException e) {
                Log.e("uri exception in getURL", "error");
                Log.e("error", e.getMessage());
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                Log.e("prot exception", "error");
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("io exception in getURL", "error");
                e.printStackTrace();
            }
            return response;
        }
        public String convertStreamToString(InputStream inputStream) throws IOException {
            if (inputStream != null) {
                Writer writer = new StringWriter();

                char[] buffer = new char[1024];
                try {
                    Reader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"),1024);
                    int n;
                    while ((n = reader.read(buffer)) != -1) {
                        writer.write(buffer, 0, n);
                    }
                } finally {
                    inputStream.close();
                }
                return writer.toString();
            } else {
                return "";
            }
        }
    }

}
