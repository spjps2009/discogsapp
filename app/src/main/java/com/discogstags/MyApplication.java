package com.discogstags;

/**
 * Created by fzaka on 4/21/2015.
 */
import android.app.Application;

import org.acra.*;
import org.acra.annotation.*;

@ReportsCrashes(formUri = "", // will not be used
        mailTo = "sp.jps2009@gmail.com",
        mode = ReportingInteractionMode.TOAST,
        resToastText = R.string.crash)

public class MyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        // The following line triggers the initialization of ACRA
        ACRA.init(this);
    }
}