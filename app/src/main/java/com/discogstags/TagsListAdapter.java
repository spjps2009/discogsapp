package com.discogstags;

import android.app.Activity;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by fzaka on 4/4/2015.
 */
public class TagsListAdapter extends BaseAdapter{
    private final Activity context;
    private ArrayList<String> tags;
    private ArrayList<Integer> tagids;
    private ArrayList<Integer> taglikes;
    private ArrayList<Integer> tagdislikes;
    private final ArrayList<String> tags_top;
    private final ArrayList<Integer> tagids_top;
    private final ArrayList<Integer> taglikes_top;
    private final ArrayList<Integer> tagdislikes_top;
    private final ArrayList<String> artists;
    private final ArrayList<String> countries;
    private final ArrayList<String> genres;
    private final ArrayList<String> styles;
    private final String year;
    private LayoutInflater inflater;
    public static boolean[] colored;

    public TagsListAdapter(Activity context, ArrayList<String> tags,
                           ArrayList<Integer> tagids,
                           ArrayList<Integer> taglikes,
                           ArrayList<Integer> tagdislikes,
                           ArrayList<String> tags_top,
                           ArrayList<Integer> tagids_top,
                           ArrayList<Integer> taglikes_top,
                           ArrayList<Integer> tagdislikes_top,
                           ArrayList<String> artists,
                           ArrayList<String> countries,
                           ArrayList<String> genres,
                           ArrayList<String> styles,
                           String year) {
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.tags = tags;
        this.tagids = tagids;
        this.taglikes = taglikes;
        this.tagdislikes = tagdislikes;

        this.tags_top = tags_top;
        this.tagids_top = tagids_top;
        this.taglikes_top = taglikes_top;
        this.tagdislikes_top = tagdislikes_top;
        this.artists = artists;
        this.countries = countries;
        this.genres = genres;
        this.styles = styles;
        this.year = year;
        this.colored = new boolean[tags_top.size() + tags.size() + 5];
    }

    @Override
    public int getCount() {
        return tags_top.size() + tags.size() + 5;
    }

    @Override
    public Object getItem(int position) {
        Log.e("id", "item id "  + position);
        return new Object();
    }

    @Override
    public long getItemId(int position) {
        int counter = 0;
        int temp = 0;
        if(position == counter){
            return 0;
        }
        counter++;
        for(int i = 0; i < artists.size(); i++, counter++){
            if(position == counter){
                return tagids_top.get(temp);
            }
            temp++;
        }
        if(position == counter){
            return 0;
        }
        counter++;
        for(int i = 0; i < countries.size(); i++, counter++){
            if(position == counter){
                return tagids_top.get(temp);
            }
            temp++;
        }
        if(position == counter){
            return 0;
        }
        counter++;
        for(int i = 0; i < genres.size();  i++, counter++){
            if(position == counter){
                return tagids_top.get(temp);
            }
            temp++;
        }
        if(position == counter){
            return 0;
        }
        counter++;
        for(int i = 0; i < styles.size();  i++, counter++){
            if(position == counter){
                return tagids_top.get(temp);
            }
            temp++;
        }
        if(position == counter){
            return 0;
        }
        counter++;
        for(int i = 0; i < 1; i++, counter++){
            if(position == counter){
                return tagids_top.get(temp);
            }
            temp++;
        }
        if(position == counter){
            return 0;
        }
        counter++;
        Log.e("counter1", position + " " + counter);
        return tagids.get(position - counter);

    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        int counter = 0;
        int temp = 0;
        if(view == null){
            Log.e("view null", "");
        }else{
            Log.e("view not null", "");
        }
//        Log.e("counter1", position + " " + counter);
        if(position == counter){
            return returnTitle( parent,  "Artist");

        }
        counter++;
        for(int i = 0; i< artists.size(); i++,counter++){
            if(position == counter){
                return returnTopTag(parent, temp, position);
            }
            temp++;
        }
        if(position == counter){
            return returnTitle( parent,  "Country");
        }
        counter++;
        for(int i = 0; i< countries.size(); i++,counter++){
            if(position == counter){
                return returnTopTag(parent, temp, position);
            }
            temp++;

        }
        if(position == counter){
            return returnTitle(parent,  "Genre");
        }
        counter++;
        for(int i = 0; i< genres.size(); i++,counter++){
            if(position == counter){
                return returnTopTag(parent, temp, position);
            }
            temp++;

        }
        if(position == counter){
            return returnTitle( parent,  "Style");
        }
        counter++;
        for(int i = 0; i< styles.size(); i++,counter++){
            if(position == counter){
                return returnTopTag(parent, temp, position);
            }
            temp++;

        }
        if(position == counter){
            return returnTitle(parent,  "Year");
        }
        counter++;
        for(int i = 0; i< 1; i++,counter++){
            if(position == counter){
                return returnTopTag(parent, temp, position);
            }
            temp++;

        }
        if(position == counter){
            return returnTitle(parent,  "Tags");
        }
        counter++;
        Log.e("counter12", position + " " + counter);
        return returnTag(parent,  position, counter);
    }
    private View returnTitle(ViewGroup parent, String title) {

        View rowView = inflater.inflate(R.layout.info_row_title, parent, false);
        TextView rowTextView = (TextView) rowView.findViewById(R.id.rowTextView);
        //CheckBox checkbox = (CheckBox) rowView.findViewById(R.id.checkbox);
//            checkbox.setOnCheckedChangeListener(this);
        rowTextView.setText(title);
        return rowView;
    }
    private View returnTag(ViewGroup parent, int position, int counter){
        View rowView= inflater.inflate(R.layout.tags_row, parent, false);
        TextView tagTitle = (TextView) rowView.findViewById(R.id.rowTextView);
        TextView likes = (TextView) rowView.findViewById(R.id.thumbsupCount);
        TextView dislikes= (TextView) rowView.findViewById(R.id.thumbsdownCount);
        Button thumbsUpButton= (Button) rowView.findViewById(R.id.thumbsup);
        thumbsUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("likes", "like");
            }
        });
        Button thumbsDownButton= (Button) rowView.findViewById(R.id.thumbsdown);
        thumbsDownButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("dislikes", "dislike");
            }
        });
        tagTitle.setText(tags.get(position - counter));
        likes.setText(taglikes.get(position - counter) + "");
        dislikes.setText(tagdislikes.get(position - counter) + "");
        if(colored[position] == true){
            Log.e("setting ", position + " to colored");
            rowView.setBackgroundColor(Color.GREEN);
        }
        return rowView;
    }


    private View returnTopTag(ViewGroup parent,  int position, int orig){
        View rowView= inflater.inflate(R.layout.tags_row, parent, false);
        TextView tagTitle = (TextView) rowView.findViewById(R.id.rowTextView);
        TextView likes = (TextView) rowView.findViewById(R.id.thumbsupCount);
        TextView dislikes= (TextView) rowView.findViewById(R.id.thumbsdownCount);
        Button thumbsUpButton= (Button) rowView.findViewById(R.id.thumbsup);
        thumbsUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("likes", "like");
            }
        });
        Button thumbsDownButton= (Button) rowView.findViewById(R.id.thumbsdown);
        thumbsDownButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("dislikes", "dislike");
            }
        });
        tagTitle.setText(tags_top.get(position));
        likes.setText(taglikes_top.get(position) + "");
        dislikes.setText(tagdislikes_top.get(position) + "");
        if(colored[orig] == true){
            Log.e("setting ", position + " to colored");
            rowView.setBackgroundColor(Color.GREEN);
        }
        return rowView;
    }
}
