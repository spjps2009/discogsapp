package com.discogstags;
/**
 * Created by varun on 4/16/2015.
 */
public class Release {
    private String releaseId;
    private String releaseName;
    private String year;
    private String country;
    private String icon;
    private String views;

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Release(String releaseId, String releaseName, String year, String country) {
        super();
        this.releaseId = releaseId;
        this.releaseName = releaseName;
        this.year = year;
        this.country = country;
    }

    public String getViews() {
        return views;
    }

    public void setViews(String views) {
        this.views = views;
    }

    public Release(String releaseId, String releaseName, String year, String country, String views) {
        super();
        this.releaseId = releaseId;
        this.releaseName = releaseName;
        this.year = year;
        this.country = country;
        this.views = views;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getReleaseName() {
        return releaseName;
    }

    public void setReleaseName(String releaseName) {
        this.releaseName = releaseName;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getReleaseId() {
        return releaseId;
    }

    public void setReleaseId(String releaseId) {
        this.releaseId = releaseId;
    }

    @Override
    public String toString() {
        return this.releaseId + " " + this.releaseName;
    }
}
