package com.discogstags;
/**
 * Created by varun on 4/19/2015.
 */

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by varun on 4/19/2015.
 */
public class MyListAdapter extends ArrayAdapter<Release> {

    public List<Release> releasesList = new ArrayList<Release>();

    public MyListAdapter(Context context, ArrayList<Release> releases) {
        super(context, 0, releases);
        releasesList = releases;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        View itemView = convertView;
        if (itemView == null){
            itemView = LayoutInflater.from(getContext()).inflate(R.layout.release, parent, false);
        }
        Release currentRelease = releasesList.get(position);
        if (currentRelease == null)
            return null;
        TextView releaseName = (TextView) itemView.findViewById(R.id.releaseName);
        releaseName.setText(currentRelease.getReleaseName());

        TextView year = (TextView) itemView.findViewById(R.id.year);
        String s = currentRelease.getYear();
        if(currentRelease.getYear() == null){
            s = "";
        }
        s = s.split("-", 0)[0];
        year.setText(s);

        TextView countryName = (TextView) itemView.findViewById(R.id.countryName);
        String s2 = currentRelease.getCountry();
        if(currentRelease.getCountry() == null){
            s2 = "";
        }
        countryName.setText(s2);
        Log.v("Error", "here");
        return itemView;
    }
}
