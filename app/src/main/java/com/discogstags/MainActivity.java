package com.discogstags;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.SearchManager;
import android.app.SearchableInfo;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.view.View;
import android.widget.EditText;
import android.os.AsyncTask;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import android.widget.ListView;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.net.URI;
import java.net.URLConnection;
import java.net.URISyntaxException;
import android.util.Log;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity {
    private List<Release> releasesList = new ArrayList<Release>();
    ArrayAdapter<Release> adapter;
    ListView list;
    MainActivity m;
    SearchView mSearchView;

    ActionBar.Tab tab1, tab2, tab3;
    Fragment fragmentTab1 = new FragmentTab1();
    Fragment fragmentTab2 = new FragmentTab2();
    Fragment fragmentTab3 = new FragmentTab3();
    private Menu menu;
    String searchString = "";
    FragmentManager fragMgr = getFragmentManager();
    FragmentTransaction fragTrans = fragMgr.beginTransaction();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        tab1 = actionBar.newTab().setText("Recommended Songs");
        tab2 = actionBar.newTab().setText("Playlist");
        tab3 = actionBar.newTab().setText("Search");

        tab1.setTabListener(new MyTabListener(fragmentTab1));
        tab2.setTabListener(new MyTabListener(fragmentTab2));
        tab3.setTabListener(new MyTabListener(fragmentTab3));

        actionBar.addTab(tab1);
        actionBar.addTab(tab2);
        actionBar.addTab(tab3);
        m = this;
    }


    public class MyTabListener implements ActionBar.TabListener {
        Fragment fragment;

        public MyTabListener(Fragment fragment) {
            this.fragment = fragment;
        }

        public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
            ft.replace(R.id.fragment_container, fragment);
        }

        public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {
            ft.remove(fragment);
        }

        public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {
            // nothing done here
        }
    }

    private class RetrieveUrl extends AsyncTask<String, Void, String> {

        private Exception exception;

        protected String doInBackground(String... urls) {
            HttpResponse response = getURL(urls[0]);
            try {
                InputStream content = response.getEntity().getContent();
                String jsonString = convertStreamToString(content);
                //return jsonString;
                //list.setAdapter(adapter);
                return jsonString;
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
        protected void onPostExecute(String result) {
            JSONObject json = null;
            try {
                json = new JSONObject(result);
                Log.e("json", json.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JSONArray releases = null;

            try {
                releases = json.getJSONArray("releases");
                String s = json.getString("query").toString();

                for(int i = 0; i < releases.length(); i++){
                    String releaseName = releases.getJSONArray(i).getString(2);
                    String year = releases.getJSONArray(i).getString(4);
                    String country = releases.getJSONArray(i).getString(3);
                    String id = releases.getJSONArray(i).getString(0);
                    Release newObj = new Release(id, releaseName, year, country);
                    releasesList.add(newObj);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            adapter = new MyListAdapter(m, (ArrayList<Release>) releasesList);

//            list = (ListView) findViewById(R.id.releasesList);
//            list.setAdapter(adapter);
//            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                @Override
//                public void onItemClick(AdapterView<?> parent, View container, int position, long id) {
//                    Release r = releasesList.get(position);
//                    Toast.makeText(getApplicationContext(), r.getReleaseId(), Toast.LENGTH_LONG).show();
//                }
//            });
        }
        private HttpResponse getURL(String url){
            HttpResponse response = null;
            try {
                HttpClient client = new DefaultHttpClient();
                HttpGet request = new HttpGet();
                request.setURI(new URI(url));
                response = client.execute(request);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return response;
        }
        public String convertStreamToString(InputStream inputStream) throws IOException {
            if (inputStream != null) {
                Writer writer = new StringWriter();

                char[] buffer = new char[1024];
                try {
                    Reader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"),1024);
                    int n;
                    while ((n = reader.read(buffer)) != -1) {
                        writer.write(buffer, 0, n);
                    }
                } finally {
                    inputStream.close();
                }
                return writer.toString();
            } else {
                return "";
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_temp, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        final SearchView searchView = (SearchView) menu.findItem(R.id.action_search)
                .getActionView();
        if (null != searchView) {
            searchView.setSearchableInfo(searchManager
                    .getSearchableInfo(getComponentName()));
            searchView.setIconifiedByDefault(false);
        }

        SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {
            public boolean onQueryTextChange(String newText) {
                // this is your adapter that will be filtered
                return true;
            }

            public boolean onQueryTextSubmit(String query) {
                //Here u can get the value "query" which is entered in the search box.
                searchString = query;
                if (getActionBar().getSelectedNavigationIndex() == 2)
                    ((FragmentTab3)fragmentTab3).update();
                else
                {
                    fragTrans.replace(R.id.fragment_container, fragmentTab3);
                    fragTrans.addToBackStack(null);
                    fragTrans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                    //fragTrans.commit();
                    getActionBar().setSelectedNavigationItem(2);
                }
                searchView.clearFocus();
                return true;
            }
        };
        searchView.setOnQueryTextListener(queryTextListener);

        return super.onCreateOptionsMenu(menu);
    }

    public String getSearchString() {
        return searchString;
    }
}
