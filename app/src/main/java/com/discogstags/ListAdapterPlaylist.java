package com.discogstags;
/**
 * Created by varun on 4/19/2015.
 */

import android.content.Context;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by varun on 4/19/2015.
 */
public class ListAdapterPlaylist extends ArrayAdapter<String> {

    public List<String> dates = new ArrayList<String>();

    public ListAdapterPlaylist(Context context, List<String> dates1) {
        super(context, 0, dates1);
        dates = dates1;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        View itemView = convertView;
        try {
            if (itemView == null) {
                itemView = LayoutInflater.from(getContext()).inflate(R.layout.playlistitem, parent, false);
            }
            String date = dates.get(position);
            if (date == null)
                return null;
            TextView releaseName = (TextView) itemView.findViewById(R.id.playlistname);
            //long longTimeAgo    = timeStringtoMilis("2004-05-12 09:33:12");
            String partialdate = removeExtension(date);
            long longTimeAgo    = timeStringtoMilis(partialdate);
            String relative = DateUtils.getRelativeDateTimeString(getContext(), longTimeAgo, DateUtils.SECOND_IN_MILLIS, DateUtils.WEEK_IN_MILLIS, DateUtils.FORMAT_ABBREV_ALL).toString();
            releaseName.setText(relative);
        }catch (Exception e)
        {
            e.printStackTrace();
        }
        return itemView;
    }
    private long timeStringtoMilis(String time) {
        long milis = 0;

        try {
            SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            sd.setTimeZone(new TimeZone() {
                @Override
                public int getOffset(int era, int year, int month, int day, int dayOfWeek, int timeOfDayMillis) {
                    return -5;
                }

                @Override
                public int getRawOffset() {
                    return 0;
                }

                @Override
                public boolean inDaylightTime(Date time) {
                    return false;
                }

                @Override
                public void setRawOffset(int offsetMillis) {

                }

                @Override
                public boolean useDaylightTime() {
                    return false;
                }
            });
            Date date 	= sd.parse(time);

            milis 		= date.getTime();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return milis;
    }
    private String removeExtension( String filename ){
        int extensionIndex = filename.lastIndexOf( '.' );
        if ( extensionIndex == -1 ) {
            return filename;
        } else {
            return filename.substring(0, extensionIndex);
        }
    }
}
