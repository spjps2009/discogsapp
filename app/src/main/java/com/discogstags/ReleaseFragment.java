package com.discogstags;

/**
 * Created by fzaka on 4/8/2015.
 */

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.discogstags.util.Utils;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;

/**
 * A placeholder fragment containing a simple view.
 */
@SuppressLint("ValidFragment")
public class ReleaseFragment extends Fragment {
    private ListView tagsListView;
    private Button skipButton, doneButton, detailsButton;
    private TagsListAdapter tagsListAdapter;
    private ArrayAdapter<String> listAdapter ;
    public static int RELEASE_ID = -1;
    String currID;
    public static ArrayList<Integer> nextReleaseId = null;
    public static boolean[] colored;
    Object syncObject = new Object();
    ArrayList<String> artists, genres, styles, countries, years;
    String label;
    String title;
    public ArrayList<Video> previousReleases;
    private int currSelected = -1;
    YouTubePlayer youTubePlayerGlobal = null;
    boolean tagRunning = false;
    boolean releaseRunning = false;
    boolean saveRunning = false;

    public ReleaseFragment() {
    }
    private class Video {
        public int RELEASE_ID;
        public String YOUTUBE_ID;
        public Video(int r, String v){
            this.RELEASE_ID = r;
            this.YOUTUBE_ID = v;
        }

        @Override
        public String toString() {
            return "{" + RELEASE_ID + "," + YOUTUBE_ID + '}';
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_release, container, false);
        previousReleases = new ArrayList<>();
        String url = "http://ec2-52-0-89-113.compute-1.amazonaws.com/home/release/" + RELEASE_ID + "/?json=true";
        new RetrieveReleaseUrl().execute(url);

        // Find the ListView resource.
        tagsListView = (ListView) rootView.findViewById( R.id.mainListView );
        skipButton = (Button) rootView.findViewById(R.id.skip);
        skipButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if(ReleaseActivity.videos != null && ReleaseActivity.videos.size() != 0) {
                        tagsListView.setAdapter(listAdapter);
                        currID = ReleaseActivity.videos.remove(0);
                        youTubePlayerGlobal.loadVideo(currID);
                    }else {
                        Log.e("prev ", "add: " + RELEASE_ID);
                        Log.e("previouses", previousReleases.toString());
                        //previousReleases.add(RELEASE_ID);
                        RELEASE_ID = nextReleaseId.remove(0);
                        ReleaseActivity.videos = null;
//                ReleaseActivity.currIndex = 0;
                        String url = "http://ec2-52-0-89-113.compute-1.amazonaws.com/home/release/" + RELEASE_ID + "/?json=true";
                        if(!releaseRunning) {
                            releaseRunning = true;
                            new RetrieveReleaseUrl().execute(url);
                        }
                    }
                }catch(Exception e){
                    String request = "";
                    Uri.Builder uribuilder = new Uri.Builder()
                            .scheme("http")
                            .authority("ec2-52-0-89-113.compute-1.amazonaws.com")
                            .appendPath("home")
                            .appendPath("more");
                    uribuilder.appendQueryParameter("style", styles.toString().replace("[", "").replace("]", "").replace(", ", ","));
                    uribuilder.appendQueryParameter("release_id", RELEASE_ID + "");
                    Uri uri = uribuilder.build();
                    String url = uri.toString();
                    Log.e("request", url);
//                    Toast.makeText(getActivity(), url,Toast.LENGTH_LONG).show();
                    if(!tagRunning) {
                        tagRunning = true;
                        RetrieveTagsUrl retrieveTagsUrl = new RetrieveTagsUrl();
                        retrieveTagsUrl.execute(url);
                    }
                }

            }
        });
        doneButton = (Button) rootView.findViewById(R.id.done);
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String request = "";

                    Uri.Builder uribuilder = new Uri.Builder()
                            .scheme("http")
                            .authority("ec2-52-0-89-113.compute-1.amazonaws.com")
                            .appendPath("home")
                            .appendPath("save");
                    String releases = "", youtubes = "";
                    for (Video vd : previousReleases) {
                        releases += vd.RELEASE_ID + ",";
                        youtubes += vd.YOUTUBE_ID + ",";

                    }
                    uribuilder.appendQueryParameter("releases", releases.substring(0, releases.length() - 1));
                    uribuilder.appendQueryParameter("youtubes", youtubes.substring(0, youtubes.length() - 1));
                    uribuilder.appendQueryParameter("userid", 1234 + "");
                    Uri uri = uribuilder.build();
                    String url = uri.toString();
                    Log.e("request", url);
//                    Toast.makeText(getActivity(), url, Toast.LENGTH_LONG).show();
                    if (!saveRunning) {
                        saveRunning = true;
                        new RetrieveSaveUrl().execute(url);
                    }
                }catch(Exception e){
                    Toast.makeText(getActivity(), "No tracks listened", Toast.LENGTH_LONG).show();
                }
            }
        });


        detailsButton = (Button) rootView.findViewById(R.id.detials);
        detailsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(getActivity())
                        .setMessage("Release Name: " + title + "\n\n" +
                                "Artists: " + artists.toString().replace("[", "").replace("]", "").replace(", ", ",")  + "\n")

                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // continue with delete
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .show();
            }
        });



        return rootView;


    }


    public boolean backButtonWasPressed() {
        try {

            Log.e("back", "button");
            RELEASE_ID = previousReleases.remove(previousReleases.size() - 1).RELEASE_ID;
            Log.e("prev ", "remove: " + RELEASE_ID);
            Log.e("previouses", previousReleases.toString());
            String url = "http://ec2-52-0-89-113.compute-1.amazonaws.com/home/release/" + RELEASE_ID + "/?json=true";
            ReleaseActivity.videos = null;
//            ReleaseActivity.currIndex = 0;
            if(!releaseRunning) {
                releaseRunning = true;
                new RetrieveReleaseUrl().execute(url);
            }
            return true;
        }catch (Exception e){
            return false;
        }
    }

    private class RetrieveReleaseUrl extends AsyncTask<String, Void, String> {
        private Exception exception;

        protected String doInBackground(String... urls) {
            HttpResponse response = getURL(urls[0]);
            Log.e("getting release url:",  urls[0]) ;
            String jsonString = null;
            try {
                InputStream content = response.getEntity().getContent();
                jsonString = convertStreamToString(content);
            } catch (IOException e) {
                e.printStackTrace();
            }
            JSONObject json = null;
            try {
                json = new JSONObject(jsonString);
                Log.e("json", json.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JSONArray videosJSON = null, tagsJSON = null, releaseJSON = null, releases_artistsJSON = null, labelJSON = null;
            try {
                videosJSON = json.getJSONArray("releases_youtube");
                tagsJSON = json.getJSONArray("tag");
                releaseJSON = json.getJSONArray("release");
                releases_artistsJSON = json.getJSONArray("releases_artists");
                labelJSON = json.getJSONArray("label");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.e("json video", videosJSON.toString());
            Log.e("json tags", tagsJSON.toString());
            Log.e("json release", releaseJSON.toString());
            Log.e("json releases_artists", releases_artistsJSON.toString());
            String country = null, date = null, genre = null, style = null;
            try {
                country = releaseJSON.getJSONArray(0).getString(3);
                date = releaseJSON.getJSONArray(0).getString(4);
                genre = releaseJSON.getJSONArray(0).getString(7);
                style = releaseJSON.getJSONArray(0).getString(8);
                label = labelJSON.getJSONArray(0).getString(0);
                title = releaseJSON.getJSONArray(0).getString(2);
                Log.e("meta", country + " " + date + " " + genre + " " + style);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.e("json youtube", videosJSON.toString());
            for(int i = 0; i < videosJSON.length(); i++){
                try {
                    if(ReleaseActivity.videos == null)
                        ReleaseActivity.videos = new ArrayList<>();
                    ReleaseActivity.videos.add(videosJSON.getJSONArray(i).getString(1).toString());
//                    ReleaseActivity.currIndex = 0;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            ArrayList<String> moreList = new ArrayList<String>();
            moreList.add("More by Artist");
            moreList.add("More like Style");
            moreList.add("More like Genre");
            moreList.add("More in Time Period");
            moreList.add("More like Label");
            moreList.add("More like Country/Style");
            moreList.add("More like Country/Genre");
            moreList.add("More in Time Period/Style");
            moreList.add("More in Time Period/Country");
            colored = new boolean[moreList.size()];
            currSelected = -1;


            // Create ArrayAdapter using the planet list.
            listAdapter = new ArrayAdapter<String>(getActivity(), R.layout.more_row, moreList);

            artists = new ArrayList<>();
            for(int i = 0; i < releases_artistsJSON.length(); i++){
                try {
                    artists.add(releases_artistsJSON.getJSONArray(i).getString(3));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            for(String a:artists){
                Log.e("artists", a);
            }
            countries = Utils.parseCountries(country);
            for(String c: countries){
                Log.e("countries", c);
            }
            genres = Utils.parseGenres(genre);
            for(String g:genres){
                Log.e("genre", g);
            }
            styles = Utils.parseStyles(style);
            for(String s:styles){
                Log.e("style", s);
            }
            years = Utils.getAllYears(date);
            tagsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    String request = "";
                    Uri.Builder uribuilder = new Uri.Builder()
                            .scheme("http")
                            .authority("ec2-52-0-89-113.compute-1.amazonaws.com")
                            .appendPath("home")
                            .appendPath("more");
                    if (position == 0) {
                        uribuilder.appendQueryParameter("artist", artists.toString().replace("[", "").replace("]", "").replace(", ", ","));
                    } else if (position == 1) {
                        uribuilder.appendQueryParameter("style", styles.toString().replace("[", "").replace("]", "").replace(", ", ","));
                    } else if (position == 2) {
                        uribuilder.appendQueryParameter("genre", genres.toString().replace("[", "").replace("]", "").replace(", ", ","));
                    } else if (position == 3) {
                        uribuilder.appendQueryParameter("time", years.toString().replace("[", "").replace("]", "").replace(", ", ","));
                    } else if (position == 4) {
                        uribuilder.appendQueryParameter("label", label);
                    } else if (position == 5) {
                        uribuilder.appendQueryParameter("country", countries.toString().replace("[", "").replace("]", "").replace(", ", ","));
                        uribuilder.appendQueryParameter("style", styles.toString().replace("[", "").replace("]", "").replace(", ", ","));
                    } else if (position == 6) {
                        uribuilder.appendQueryParameter("country", countries.toString().replace("[", "").replace("]", "").replace(", ", ","));
                        uribuilder.appendQueryParameter("genre", genres.toString().replace("[", "").replace("]", "").replace(", ", ","));
                    } else if (position == 7) {
                        uribuilder.appendQueryParameter("time", years.toString().replace("[", "").replace("]", "").replace(", ", ","));
                        uribuilder.appendQueryParameter("style", styles.toString().replace("[", "").replace("]", "").replace(", ", ","));
                    } else if (position == 8) {
                        uribuilder.appendQueryParameter("time", years.toString().replace("[", "").replace("]", "").replace(", ", ","));
                        uribuilder.appendQueryParameter("country", countries.toString().replace("[", "").replace("]", "").replace(", ", ","));
                    }
                    //Long tag_id = parent.getItemIdAtPosition(position);
                    //String url = "http://ec2-52-0-89-113.compute-1.amazonaws.com/home/more/?" + request;
                    uribuilder.appendQueryParameter("release_id", RELEASE_ID + "");
                    Uri uri = uribuilder.build();
                    String url = uri.toString();
                    Log.e("request", url);
//                    Toast.makeText(getActivity(), url,Toast.LENGTH_LONG).show();
                    currSelected = position;
                    new RetrieveTagsUrl().execute(url);
                    int count = parent.getCount();
                    for (int i = 0; i < count; i++) {
                        if (i == position) {
                            colored[i] = true;
                            view.setBackgroundColor(Color.RED);
                        } else {
                            if (parent.getChildAt(i) == null) {
                                Log.e("child at ", i + " is null");
                            } else {
                                parent.getChildAt(i).setBackgroundColor(Color.WHITE);
                                Log.e("child at ", i + " is not null");
                            }
                            colored[i] = false;
                        }
                    }
                    Log.e("clicked", "on pos: " + position);

                }
            });

            Log.e("done", "done");
            synchronized(syncObject) {
                Log.e("notify", "notify");
                syncObject.notify();
            }
            return null;
        }
        protected void onPostExecute(String result) {
            try {
                releaseRunning = false;
                tagsListView.setAdapter(listAdapter);
                ReleaseActivity.youTubePlayerFragment.onDestroy();
                ReleaseActivity.youTubePlayerFragment.initialize(ReleaseActivity.DEVELOPER_KEY, new YouTubePlayer.OnInitializedListener() {
                    @Override
                    public void onInitializationSuccess(YouTubePlayer.Provider provider, final YouTubePlayer youTubePlayer, boolean b) {
//                    Log.e("currindex", ReleaseActivity.currIndex + "");
                        youTubePlayerGlobal = youTubePlayer;
                        String temp = ReleaseActivity.videos.remove(0);
                        youTubePlayer.loadVideo(temp);
                        currID = temp;
                        youTubePlayer.setPlayerStateChangeListener(new YouTubePlayer.PlayerStateChangeListener() {
                            @Override
                            public void onLoading() {
                                Log.e("player", "loading");
                            }

                            @Override
                            public void onLoaded(String s) {
                                Log.e("player", "loaded");
                            }

                            @Override
                            public void onAdStarted() {
                                Log.e("player", "ad");
                            }

                            @Override
                            public void onVideoStarted() {
//                                Toast.makeText(getActivity(), "Current ReleaseID " + RELEASE_ID, Toast.LENGTH_LONG).show();
                                if ((nextReleaseId == null || nextReleaseId.size() == 0) && ReleaseActivity.videos.size() == 0) {
                                    String request = "";
                                    Uri.Builder uribuilder = new Uri.Builder()
                                            .scheme("http")
                                            .authority("ec2-52-0-89-113.compute-1.amazonaws.com")
                                            .appendPath("home")
                                            .appendPath("more");
                                    uribuilder.appendQueryParameter("style", styles.toString().replace("[", "").replace("]", "").replace(", ", ","));
                                    uribuilder.appendQueryParameter("release_id", RELEASE_ID + "");
                                    Uri uri = uribuilder.build();
                                    String url = uri.toString();
                                    Log.e("request", url);
//                                    Toast.makeText(getActivity(), url, Toast.LENGTH_LONG).show();
                                    if (!tagRunning) {
                                        tagRunning = true;
                                        new RetrieveTagsUrl().execute(url);
                                    }
                                }
                                Log.e("player", "started");
                            }

                            @Override
                            public void onVideoEnded() {
                                Log.e("player", "ended");
                                Uri.Builder uribuilder = new Uri.Builder()
                                        .scheme("http")
                                        .authority("ec2-52-0-89-113.compute-1.amazonaws.com")
                                        .appendPath("home")
                                        .appendPath("getstyles");
                                uribuilder.appendQueryParameter("releaseid", RELEASE_ID + "");
                                Uri uri = uribuilder.build();
                                String url = uri.toString();
                                new RetrieveGameUrl().execute(url);


                                final Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (nextReleaseId == null || nextReleaseId.size() == 0) {
//                                            Toast.makeText(getActivity(), "Video Ended Case 1", Toast.LENGTH_LONG).show();
                                            String request = "";
                                            Uri.Builder uribuilder = new Uri.Builder()
                                                    .scheme("http")
                                                    .authority("ec2-52-0-89-113.compute-1.amazonaws.com")
                                                    .appendPath("home")
                                                    .appendPath("more");
                                            uribuilder.appendQueryParameter("style", styles.toString().replace("[", "").replace("]", "").replace(", ", ","));
                                            uribuilder.appendQueryParameter("release_id", RELEASE_ID + "");
                                            Uri uri = uribuilder.build();
                                            String url = uri.toString();
                                            Log.e("request", url);
//                                            Toast.makeText(getActivity(), url, Toast.LENGTH_LONG).show();
                                            if (!tagRunning) {
                                                tagRunning = true;
                                                new RetrieveTagsUrl().execute(url);
                                            }
                                            if (ReleaseActivity.videos != null && ReleaseActivity.videos.size() != 0) {
//                                                Toast.makeText(getActivity(), "Video Ended Case 1.1", Toast.LENGTH_LONG).show();
                                                previousReleases.add(new Video(RELEASE_ID, currID));
                                                String temp = ReleaseActivity.videos.remove(0);
                                                youTubePlayer.loadVideo(temp);
                                                currID = temp;
                                            } else {
//                                                Toast.makeText(getActivity(), "Video Ended Case 1.2", Toast.LENGTH_LONG).show();
                                            }
                                            return;
                                        }
                                        if (ReleaseActivity.videos == null || ReleaseActivity.videos.size() == 0) {
//                                            Toast.makeText(getActivity(), "Video Ended Case 2", Toast.LENGTH_LONG).show();
                                            Log.e("prev ", "add: " + RELEASE_ID);
                                            Log.e("previouses", previousReleases.toString());
                                            previousReleases.add(new Video(RELEASE_ID, currID));
                                            RELEASE_ID = nextReleaseId.remove(0);
                                            String url = "http://ec2-52-0-89-113.compute-1.amazonaws.com/home/release/" + RELEASE_ID + "/?json=true";
                                            if (!releaseRunning) {
                                                releaseRunning = true;
                                                new RetrieveReleaseUrl().execute(url);
                                            }

                                        } else {
//                                            Toast.makeText(getActivity(), "Video Ended Case 3", Toast.LENGTH_LONG).show();
                                            //ReleaseActivity.currIndex++;
                                            previousReleases.add(new Video(RELEASE_ID, currID));
                                            String temp = ReleaseActivity.videos.remove(0);
                                            youTubePlayer.loadVideo(temp);
                                            currID = temp;
                                        }
                                    }
                                }, 5000);

                            }

                            @Override
                            public void onError(YouTubePlayer.ErrorReason errorReason) {
                                Log.e("player", "error");
                                try {
                                    RELEASE_ID = nextReleaseId.remove(0);
                                    Log.e("prev ", "remove: " + RELEASE_ID);
                                    Log.e("previouses", previousReleases.toString());
                                    String url = "http://ec2-52-0-89-113.compute-1.amazonaws.com/home/release/" + RELEASE_ID + "/?json=true";
                                    ReleaseActivity.videos = null;
//                                ReleaseActivity.currIndex = 0;
                                    if (!releaseRunning) {
                                        releaseRunning = true;
                                        new RetrieveReleaseUrl().execute(url);
                                    }
                                } catch (Exception e) {

                                }
                            }
                        });
                    }

                    @Override
                    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
                        Log.e("error", "error");
                    }
                });
            /*
            // Create and populate a List of planet names.
            ArrayList<String>  tags = new ArrayList<String>();
            ArrayList<Integer>  tagids = new ArrayList<Integer>();
            ArrayList<Integer> taglikes = new ArrayList<Integer>();
            ArrayList<Integer> tagdislikes = new ArrayList<Integer>();
            for(int i = 0; i < tagsJSON.length(); i++){
                try {
                    tags.add(tagsJSON.getJSONArray(i).getString(1));
                    tagids.add(tagsJSON.getJSONArray(i).getInt(0));
                    taglikes.add(tagsJSON.getJSONArray(i).getInt(2));
                    tagdislikes.add(tagsJSON.getJSONArray(i).getInt(3));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            ArrayList<String>  tags_top = new ArrayList<String>();
            ArrayList<Integer>  tagids_top = new ArrayList<Integer>();
            ArrayList<Integer> taglikes_top = new ArrayList<Integer>();
            ArrayList<Integer> tagdislikes_top = new ArrayList<Integer>();
            Iterator<String> iter = tags.iterator();
            while (iter.hasNext()) {
                String tag = iter.next();
                if(artists.contains(tag)){
                    int tagpos = tags.indexOf(tag);
                    tags_top.add(tag);
                    tagids_top.add(tagids.get(tagpos));
                    taglikes_top.add(taglikes.get(tagpos));
                    tagdislikes_top.add(tagdislikes.get(tagpos));
                    iter.remove();
                    tagids.remove(tagpos);
                    taglikes.remove(tagpos);
                    tagdislikes.remove(tagpos);

                }
            }
            iter = tags.iterator();
            while (iter.hasNext()) {
                String tag = iter.next();
                if(countries.contains(tag)){
                    int tagpos = tags.indexOf(tag);
                    tags_top.add(tag);
                    tagids_top.add(tagids.get(tagpos));
                    taglikes_top.add(taglikes.get(tagpos));
                    tagdislikes_top.add(tagdislikes.get(tagpos));
                    iter.remove();
                    tagids.remove(tagpos);
                    taglikes.remove(tagpos);
                    tagdislikes.remove(tagpos);

                }
            }
            iter = tags.iterator();
            while (iter.hasNext()) {
                String tag = iter.next();
                if(genres.contains(tag)){
                    int tagpos = tags.indexOf(tag);
                    tags_top.add(tag);
                    tagids_top.add(tagids.get(tagpos));
                    taglikes_top.add(taglikes.get(tagpos));
                    tagdislikes_top.add(tagdislikes.get(tagpos));
                    iter.remove();
                    tagids.remove(tagpos);
                    taglikes.remove(tagpos);
                    tagdislikes.remove(tagpos);

                }
            }
            iter = tags.iterator();
            while (iter.hasNext()) {
                String tag = iter.next();
                if(styles.contains(tag)){
                    int tagpos = tags.indexOf(tag);
                    tags_top.add(tag);
                    tagids_top.add(tagids.get(tagpos));
                    taglikes_top.add(taglikes.get(tagpos));
                    tagdislikes_top.add(tagdislikes.get(tagpos));
                    iter.remove();
                    tagids.remove(tagpos);
                    taglikes.remove(tagpos);
                    tagdislikes.remove(tagpos);

                }
            }
            iter = tags.iterator();
            while (iter.hasNext()) {
                String tag = iter.next();
                if(year.equals(tag)){
                    int tagpos = tags.indexOf(tag);
                    tags_top.add(tag);
                    tagids_top.add(tagids.get(tagpos));
                    taglikes_top.add(taglikes.get(tagpos));
                    tagdislikes_top.add(tagdislikes.get(tagpos));
                    iter.remove();
                    tagids.remove(tagpos);
                    taglikes.remove(tagpos);
                    tagdislikes.remove(tagpos);

                }
            }
            iter = tags.iterator();
            while (iter.hasNext()) {
                String tag = iter.next();
                int pos = tags.indexOf(tag);
                String yearTag = tag.replaceAll("\\D+","");
                if(!yearTag.equals("")){
                    String yearString = getStringYear(Integer.parseInt(yearTag));
                    Log.e("tag", yearString + " " + tag);
                    tags.set(pos, tag.replace(yearTag, yearString));
                }
            }
            iter = tags_top.iterator();
            while (iter.hasNext()) {
                String tag = iter.next();
                int pos = tags_top.indexOf(tag);
                String yearTag = tag.replaceAll("\\D+","");
                if(!yearTag.equals("")){
                    String yearString = getStringYear(Integer.parseInt(yearTag));
                    Log.e("tag", yearString + " " + tag);
                    tags_top.set(pos, tag.replace(yearTag, yearString));
                }
            }
            tagsListAdapter= new TagsListAdapter(getActivity(), tags, tagids, taglikes, tagdislikes, tags_top, tagids_top, taglikes_top, tagdislikes_top, artists,countries, genres, styles, year);
            */
//            tagsListView.setAdapter(tagsListAdapter);

            }catch (Exception e){
                e.printStackTrace();
            }

        }

        private String getStringYear(int year){
            if(year >=2010){
                return year + "";
            }else if(year >= 2000 && year < 2005){
                return "Early 2000s";
            }else if(year >= 2005 && year < 2010){
                return "Late 2000s";
            }else if(year < 2000){
                int decade = getNthDigit(year, 10, 2)*10;
                int yearDigit = getNthDigit(year, 10, 1);
                if(yearDigit > 5){
                    return "Late " + decade + "s";
                }else{
                    return "Early " + decade + "s";
                }
            }
            return year + "";
        }

        public int getNthDigit(int number, int base, int n) {
            return (int) ((number / Math.pow(base, n - 1)) % base);
        }

        private HttpResponse getURL(String url){
            HttpResponse response = null;
            try {
                HttpClient client = new DefaultHttpClient();
                HttpGet request = new HttpGet();
                request.setURI(new URI(url));
                response = client.execute(request);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return response;
        }
        public String convertStreamToString(InputStream inputStream) throws IOException {
            if (inputStream != null) {
                Writer writer = new StringWriter();

                char[] buffer = new char[1024];
                try {
                    Reader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"),1024);
                    int n;
                    while ((n = reader.read(buffer)) != -1) {
                        writer.write(buffer, 0, n);
                    }
                } finally {
                    inputStream.close();
                }
                return writer.toString();
            } else {
                return "";
            }
        }
    }
    private class RetrieveTagsUrl extends AsyncTask<String, Void, String> {

        private Exception exception;

        protected String doInBackground(String... urls) {
            Log.e("url", urls[0]);
            HttpResponse response = getURL(urls[0]);
            Log.e("response", response.getStatusLine().toString());
            try {
                InputStream content = response.getEntity().getContent();
                String jsonString = convertStreamToString(content);
                return jsonString;
            } catch (IOException e) {
                Log.e("error in do in bg", e.getMessage());
            }
            return null;
        }
        protected void onPostExecute(String result) {
            releaseRunning = false;
            try {
//                Toast.makeText(getActivity(), result,Toast.LENGTH_LONG).show();
                JSONObject json = null;
                try {
                    json = new JSONObject(result);
                    Log.e("json", json.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                JSONArray releasesJSON = null;
                try {
                    releasesJSON = json.getJSONArray("tracks");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.e("json video", releasesJSON.toString());
                nextReleaseId = new ArrayList<>();
                for (int i = 0; i < releasesJSON.length(); i++) {
                    try {
                        //Log.e("id", releasesJSON.getJSONArray(i).getInt(0) + "");
                        nextReleaseId.add(releasesJSON.getJSONArray(i).getInt(0));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                ReleaseActivity.videos = null;
//            ReleaseActivity.currIndex = 0;
                Log.e("nextReleaseID", nextReleaseId.toString());
                try {
                    tagsListView.getChildAt(currSelected).setBackgroundColor(Color.GREEN);
                } catch (Exception e) {

                }
//                Toast.makeText(getActivity(), "ready", Toast.LENGTH_LONG).show();
            }catch(Exception e){
                String request = "";
                Uri.Builder uribuilder = new Uri.Builder()
                        .scheme("http")
                        .authority("ec2-52-0-89-113.compute-1.amazonaws.com")
                        .appendPath("home")
                        .appendPath("more");
                uribuilder.appendQueryParameter("style", styles.toString().replace("[", "").replace("]", "").replace(", ", ","));
                uribuilder.appendQueryParameter("release_id", RELEASE_ID + "");
                Uri uri = uribuilder.build();
                String url = uri.toString();
                Log.e("request", url);
                if(!releaseRunning) {
                    releaseRunning = true;
                    new RetrieveTagsUrl().execute(url);
                }
            }

        }

        private HttpResponse getURL(String url){
            HttpResponse response = null;
            try {
                HttpClient client = new DefaultHttpClient();
                HttpGet request = new HttpGet();
                URI uri = new URI(url);
                request.setURI(uri);
                response = client.execute(request);
            }catch (URISyntaxException e) {
                Log.e("uri exception in getURL", "error");
                Log.e("error", e.getMessage());
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                Log.e("prot exception", "error");
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("io exception in getURL", "error");
                e.printStackTrace();
            }
            return response;
        }
        public String convertStreamToString(InputStream inputStream) throws IOException {
            if (inputStream != null) {
                Writer writer = new StringWriter();

                char[] buffer = new char[1024];
                try {
                    Reader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"),1024);
                    int n;
                    while ((n = reader.read(buffer)) != -1) {
                        writer.write(buffer, 0, n);
                    }
                } finally {
                    inputStream.close();
                }
                return writer.toString();
            } else {
                return "";
            }
        }
    }

    private class RetrieveSaveUrl extends AsyncTask<String, Void, String> {

        private Exception exception;

        protected String doInBackground(String... urls) {
            Log.e("url", urls[0]);
            HttpResponse response = getURL(urls[0]);
            Log.e("response", response.getStatusLine().toString());
            try {
                InputStream content = response.getEntity().getContent();
                String jsonString = convertStreamToString(content);
                return jsonString;
            } catch (IOException e) {
                Log.e("error in do in bg", e.getMessage());
            }
            return null;
        }
        protected void onPostExecute(String result) {
            saveRunning = false;
            Log.e("saved", "playlist");
            Toast.makeText(getActivity(), "Saved Playlist",Toast.LENGTH_LONG).show();
            getActivity().finish();

        }

        private HttpResponse getURL(String url){
            HttpResponse response = null;
            try {
                HttpClient client = new DefaultHttpClient();
                HttpGet request = new HttpGet();
                URI uri = new URI(url);
                request.setURI(uri);
                response = client.execute(request);
            }catch (URISyntaxException e) {
                Log.e("uri exception in getURL", "error");
                Log.e("error", e.getMessage());
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                Log.e("prot exception", "error");
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("io exception in getURL", "error");
                e.printStackTrace();
            }
            return response;
        }
        public String convertStreamToString(InputStream inputStream) throws IOException {
            if (inputStream != null) {
                Writer writer = new StringWriter();

                char[] buffer = new char[1024];
                try {
                    Reader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"),1024);
                    int n;
                    while ((n = reader.read(buffer)) != -1) {
                        writer.write(buffer, 0, n);
                    }
                } finally {
                    inputStream.close();
                }
                return writer.toString();
            } else {
                return "";
            }
        }
    }

    private class RetrieveGameUrl extends AsyncTask<String, Void, String> {

        private Exception exception;

        protected String doInBackground(String... urls) {
            Log.e("url", urls[0]);
            HttpResponse response = getURL(urls[0]);
            Log.e("response", response.getStatusLine().toString());
            try {
                InputStream content = response.getEntity().getContent();
                String jsonString = convertStreamToString(content);
                return jsonString;
            } catch (IOException e) {
                Log.e("error in do in bg", e.getMessage());
            }
            return null;
        }
        protected void onPostExecute(String result) {
//            Toast.makeText(getActivity(), "options " + result,Toast.LENGTH_LONG).show();
            Log.e("saved", "playlist");
            JSONObject json = null;
            try {
                json = new JSONObject(result);
                Log.e("json", json.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JSONArray optionsJSON = null;
            if(json == null){
                return;
            }
            try {
                optionsJSON = json.getJSONArray("options");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            final String[] items = new String[optionsJSON.length()];
            for(int i = 0; i < optionsJSON.length(); i++){
                try {
                    items[i] = optionsJSON.getString(i);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            new AlertDialog.Builder(getActivity())
                    .setSingleChoiceItems(items, 0, null)
                    .setPositiveButton(R.string.ok_button_label, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            dialog.dismiss();
                            int selectedPosition = ((AlertDialog) dialog).getListView().getCheckedItemPosition();
                            String style = items[selectedPosition];
                            Uri.Builder uribuilder = new Uri.Builder()
                                    .scheme("http")
                                    .authority("ec2-52-0-89-113.compute-1.amazonaws.com")
                                    .appendPath("home")
                                    .appendPath("styleclick");
                            uribuilder.appendQueryParameter("tag", "'" + style + "'");
                            uribuilder.appendQueryParameter("releaseid", RELEASE_ID + "");
                            uribuilder.appendQueryParameter("userid", 1234 + "");
                            Uri uri = uribuilder.build();
                            String url = uri.toString();
                            new RetrieveGameSaveUrl().execute(url);
                            Log.e("request", url);
                            // Do something useful withe the position of the selected radio button
                        }
                    })
                    .show();

        }

        private HttpResponse getURL(String url){
            HttpResponse response = null;
            try {
                HttpClient client = new DefaultHttpClient();
                HttpGet request = new HttpGet();
                URI uri = new URI(url);
                request.setURI(uri);
                response = client.execute(request);
            }catch (URISyntaxException e) {
                Log.e("uri exception in getURL", "error");
                Log.e("error", e.getMessage());
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                Log.e("prot exception", "error");
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("io exception in getURL", "error");
                e.printStackTrace();
            }
            return response;
        }
        public String convertStreamToString(InputStream inputStream) throws IOException {
            if (inputStream != null) {
                Writer writer = new StringWriter();

                char[] buffer = new char[1024];
                try {
                    Reader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"),1024);
                    int n;
                    while ((n = reader.read(buffer)) != -1) {
                        writer.write(buffer, 0, n);
                    }
                } finally {
                    inputStream.close();
                }
                return writer.toString();
            } else {
                return "";
            }
        }
    }

    private class RetrieveGameSaveUrl extends AsyncTask<String, Void, String> {

        private Exception exception;

        protected String doInBackground(String... urls) {
            Log.e("url", urls[0]);
            HttpResponse response = getURL(urls[0]);
            Log.e("response", response.getStatusLine().toString());
            try {
                InputStream content = response.getEntity().getContent();
                String jsonString = convertStreamToString(content);
                return jsonString;
            } catch (IOException e) {
                Log.e("error in do in bg", e.getMessage());
            }
            return null;
        }
        protected void onPostExecute(String result) {

        }

        private HttpResponse getURL(String url){
            HttpResponse response = null;
            try {
                HttpClient client = new DefaultHttpClient();
                HttpGet request = new HttpGet();
                URI uri = new URI(url);
                request.setURI(uri);
                response = client.execute(request);
            }catch (URISyntaxException e) {
                Log.e("uri exception in getURL", "error");
                Log.e("error", e.getMessage());
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                Log.e("prot exception", "error");
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("io exception in getURL", "error");
                e.printStackTrace();
            }
            return response;
        }
        public String convertStreamToString(InputStream inputStream) throws IOException {
            if (inputStream != null) {
                Writer writer = new StringWriter();

                char[] buffer = new char[1024];
                try {
                    Reader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"),1024);
                    int n;
                    while ((n = reader.read(buffer)) != -1) {
                        writer.write(buffer, 0, n);
                    }
                } finally {
                    inputStream.close();
                }
                return writer.toString();
            } else {
                return "";
            }
        }
    }
}