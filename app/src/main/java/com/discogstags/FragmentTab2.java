package com.discogstags;
import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.view.View;
import android.widget.EditText;
import android.os.AsyncTask;
import android.widget.LinearLayout;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import android.widget.ListView;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.net.URI;
import java.net.URLConnection;
import java.net.URISyntaxException;
import android.util.Log;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by varun on 4/21/2015.
 */

public class FragmentTab2 extends Fragment {

    ArrayAdapter<String> adapter;
    ListView list;

    private List<String> dates = new ArrayList<String>();
    private List<String> playlists = new ArrayList<String>();
    View v;
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.playlist, container, false);
        setData();
        addListenerOnButton();
        v = view;
        return view;
    }

    public void setData()
    {
        Uri.Builder builder = new Uri.Builder();

        builder.scheme("http")
                .authority("ec2-52-0-89-113.compute-1.amazonaws.com")
                .appendPath("home")
                .appendPath("playlists")
                .appendQueryParameter("userid", "1234")
                .appendQueryParameter("json", "true");

        //String url = "http://ec2-52-0-89-113.compute-1.amazonaws.com:8000/home/searchall/?q=" + searchTerm + "&limit=10000&json=true";
        new RetrieveUrl().execute(builder.build().toString());
    }

    public void addListenerOnButton() {

    }

    private class RetrieveUrl extends AsyncTask<String, Void, String> {

        private Exception exception;

        protected String doInBackground(String... urls) {
            HttpResponse response = getURL(urls[0]);
            try {
                InputStream content = response.getEntity().getContent();
                String jsonString = convertStreamToString(content);
                //return jsonString;
                //list.setAdapter(adapter);
                return jsonString;
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
        protected void onPostExecute(String result) {
            JSONObject json = null;
            try {
                json = new JSONObject(result);
                Log.e("json", json.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JSONArray releases = null;

            try {
                releases = json.getJSONArray("playlists");
                for(int i = 0; i < releases.length(); i++){
                    String date = releases.getJSONArray(i).getString(2);
                    if (!dates.contains(date)) {
                        dates.add(date);
                        playlists.add(releases.getJSONArray(i).getString(1));
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                adapter = new ListAdapterPlaylist(getActivity(), (ArrayList<String>) dates);

                list = (ListView) getView().findViewById(R.id.playlists);
                list.setAdapter(adapter);
                list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View container, int position, long id) {
                        String r = playlists.get(position);
                        Toast.makeText(getActivity(), r, Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(getActivity(), ReleaseActivity.class);
                        Bundle b = new Bundle();
                        b.putInt("playlist_id",Integer.parseInt(r)); //Your id
                        intent.putExtras(b); //Put your id to your next Intent
                        startActivity(intent);
                    }
                });
            }catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        private HttpResponse getURL(String url){
            HttpResponse response = null;
            try {
                HttpClient client = new DefaultHttpClient();
                HttpGet request = new HttpGet();
                request.setURI(new URI(url));
                response = client.execute(request);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return response;
        }
        public String convertStreamToString(InputStream inputStream) throws IOException {
            if (inputStream != null) {
                Writer writer = new StringWriter();

                char[] buffer = new char[1024];
                try {
                    Reader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"),1024);
                    int n;
                    while ((n = reader.read(buffer)) != -1) {
                        writer.write(buffer, 0, n);
                    }
                } finally {
                    inputStream.close();
                }
                return writer.toString();
            } else {
                return "";
            }
        }
    }
}
