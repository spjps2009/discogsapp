package com.discogstags.util;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by fzaka on 4/7/2015.
 */
public class Utils {
    public static ArrayList<String> parseCountries(String country){
        String[] countriesArr = country.split("\\P{Alpha}+");
        return new ArrayList<String>(Arrays.asList(countriesArr));
    }

    public static ArrayList<String> parseGenres(String genre){
        String[] genresArr = genre.split(",");
        ArrayList<String> genres = new ArrayList<>();
        for (String g:genresArr){
            genres.add(g.replaceAll("[^A-Za-z0-9]", ""));
        }
        return genres;
    }

    public static ArrayList<String> parseStyles(String style){
        String[] stylesArr = style.split(",");
        ArrayList<String> styles = new ArrayList<>();
        for (String g:stylesArr){
            styles.add(g.replaceAll("[^A-Za-z0-9]", ""));
        }
        return styles;
    }

    public static String parseYear(String year){
        if(year.length() == 4){
            return year;
        }else{
            return year.substring(0, 4);
        }

    }
    public static ArrayList<String> getAllYears(String syear){
        int year;
        try {
            if (syear.length() == 4) {
                year = Integer.parseInt(syear);
            } else {
                year = Integer.parseInt(syear.substring(0, 4));
            }
            ArrayList<String> temp = new ArrayList<>();
            if (year >= 2010) {
                temp.add(year + "");
            } else if (year >= 2000 && year < 2005) {
                return range(2000, 2004);
            } else if (year >= 2005 && year < 2010) {
                return range(2005, 2010);
            } else if (year < 2000) {
                int milcade = getNthDigit(year, 10, 4);
                int century = getNthDigit(year, 10, 3);
                int decade = getNthDigit(year, 10, 2);
                int yearDigit = getNthDigit(year, 10, 1);
                if (yearDigit >= 5) {
                    return range(Integer.parseInt(milcade + "" + century + "" + decade + "" + 5), Integer.parseInt(milcade + "" + century + "" + decade + "" + 9));
                    //return "Late " + decade + "s";
                } else {
                    return range(Integer.parseInt(milcade + "" + century + "" + decade + "" + 0), Integer.parseInt(milcade + "" + century + "" + decade + "" + 4));
                    //return "Early " + decade + "s";
                }
            }
            return temp;
        }catch (Exception e){
            return range(1950, 2010);
        }
    }
    public static ArrayList<String> range(int min, int max) {
        ArrayList<String> list = new ArrayList<>();
        for (int i = min; i <= max; i++) {
            list.add(i + "");
        }

        return list;
    }
    public static int getNthDigit(int number, int base, int n) {
        return (int) ((number / Math.pow(base, n - 1)) % base);
    }
}
